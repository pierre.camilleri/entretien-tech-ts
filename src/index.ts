import sortByRelevance from "./sort"
import { Program, ProgramNature, Sector } from "./types"

const programs: Program[] = [
    {
        title: "Tremplin",
        nature: ProgramNature.Funding,
        sectors: [
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
    },
    {
        title: "Fonds tourisme durable",
        nature: ProgramNature.Support,
        sectors: [Sector.Tourism],
        cost: "Gratuit",
    },
    {
        title: "Prêt vert ADEME",
        nature: ProgramNature.Loan,
        sectors: [Sector.Craftsmanship, Sector.Industry],
    },
    {
        title: "Aides au réemploi des emballages",
        nature: ProgramNature.Funding,
        sectors: [
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
    },
    {
        title: "Formations RSE",
        nature: ProgramNature.Training,
        sectors: [
            Sector.Tertiary,
            Sector.Tourism,
            Sector.Craftsmanship,
            Sector.Industry,
        ],
        cost: "Gratuit (sauf en Bretagne et Grand Est)",
    },
    {
        title: "Réparacteur",
        nature: ProgramNature.Training,
        sectors: [Sector.Craftsmanship],
        cost: "Gratuit",
    },
    {
        title: "Coup de pouce chauffage",
        nature: ProgramNature.Support,
        sectors: [Sector.Tertiary],
        cost: "2000 €",
    },
]

const sortedPrograms = sortByRelevance(programs)

console.log("Liste des programmes triés :")
console.log()
console.log(sortedPrograms)
