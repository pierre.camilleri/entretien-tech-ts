import { Program } from "./types"

/** Cette fonction tri les programmes par pertinence de la manière suivante :
 * 1/ les financements
 * 2/ les prêts
 * 3/ les accompagnements et formations payantes (c'est-à-dire, à l'exclusion de 4/ et 5/)
 * 4/ les accompagnements et les formations gratuits sous conditions (coût reste à charge `cost` contient le mot "gratuit" ou "Gratuit").
 * 5/ les accompagnements et les formations gratuits (coût reste à charge `cost` prend la valeur "gratuit" ou "Gratuit")
 */
export default function sortByRelevance(_programs: Program[]) {
    return []
}
