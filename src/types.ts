export type Program = {
    title: string
    sectors: Sector[]
} & (SupportOrTraining | FundingOrLoan)

interface SupportOrTraining {
    nature: ProgramNature.Support | ProgramNature.Training
    cost: string
}

interface FundingOrLoan {
    nature: ProgramNature.Funding | ProgramNature.Loan
}

export enum ProgramNature {
    Funding = "financement",
    Support = "accompagnement",
    Training = "formation",
    Loan = "prêt",
}

export enum Sector {
    Craftsmanship = "artisanat",
    Industry = "industrie",
    Tourism = "tourisme",
    Tertiary = "tertiaire",
}
