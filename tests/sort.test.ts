import sortByRelevance from "../src/sort"
import { Program, ProgramNature, Sector } from "../src/types"

describe("Example tests", () => {
    test("1 + 2 = 3", () => {
        expect(1 + 2).toBe(3)
    })
})

describe(`
  GIVEN a list of programs
  WHEN I sort them by relevance
  EXPECT the programs be in the expected order`, () => {})
